{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  };

  outputs = { self, nixpkgs }: let
    systems = [
      "aarch64-linux"
      "x86_64-linux"
      "x86_64-darwin"
      "aarch64-darwin"
    ];

    forAllSystems = nixpkgs.lib.genAttrs systems;
  in {
    packages = forAllSystems (system: let
      pkgs = import nixpkgs { inherit system; };

      desktopItem = ''
        [Desktop Entry]
        Categories=Network
        Exec=YouTubeTV
        GenericName=YouTubeTV
        Icon=YouTubeTV
        Keywords=youtube;tv
        Name=YouTubeTV
        StartupWMClass=YouTubeTV
        Type=Application
        Comment=Youtube in Tv mode
      '';

      YouTubeTV = pkgs.buildNpmPackage {
        name = "YouTubeTV";

        src = ./.;

        npmDepsHash = "sha256-Pr36AI+z8zOl9cuyFY5ff45Pn/fdZJBLNVBT6BIi3FI=";

        npmFlags = [
          "--ignore-scripts"
        ];

        dontNpmBuild = true;

        buildPhase = ''
          mkdir -p $out/bin
          mkdir -p $out/share/applications
          mkdir -p $out/share/icons/hicolor/256x256/apps

          echo "${desktopItem}" > $out/share/applications/YouTubeTV.desktop
          ln -s ${./build/icon.png} $out/share/icons/hicolor/256x256/apps/YouTubeTV.png

          echo "#!${pkgs.bash}/bin/bash
            ${pkgs.electron}/bin/electron $out/lib/node_modules/youtube-tv/main.js" > $out/bin/YouTubeTV

          chmod +x $out/bin/YouTubeTV
        '';
      };
    in {
      inherit YouTubeTV;

      default = YouTubeTV;
    });

    overlays = let
      YouTubeTV = final: prev: {
        YouTubeTV = self.packages.${prev.system}.YouTubeTV;
      };
    in {
      inherit YouTubeTV;

      default = YouTubeTV;
    };
  };
}
