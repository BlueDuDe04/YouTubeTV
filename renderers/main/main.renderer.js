"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Renderer = void 0;
var promises_1 = require("fs/promises");
var os_1 = require("os");
var process_1 = require("process");
var path_1 = require("path");
var settings_renderer_1 = require("../settings/settings.renderer");
var electron_1 = require("electron");
var Renderer = /** @class */ (function () {
    function Renderer() {
        var _this = this;
        /** userAgent allowed by YouTube TV. */
        this.userAgent = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.77 Large Screen Safari/534.24 GoogleTV/092754';
        /** Cursor visibility flag. */
        this._cursor = false;
        /** YouTube TV url with path/params */
        this._url = 'https://www.youtube.com/tv?';
        /** JavaScript injection code */
        this.jsic = '';
        /** JavaScript injection title bar styles */
        this.titleBar = '';
        // Set app menu to null.
        electron_1.Menu.setApplicationMenu(null);
        electron_1.app.on('ready', function () {
            _this.createWindow();
            _this.listenWindowMoveEvents();
            _this.url = '__DFT__';
            _this.window.webContents.on('dom-ready', function () { return _this.injectJSCode.bind(_this); });
            _this.setAccelerators();
            if ((0, os_1.platform)() === 'darwin') {
                _this.window.on('enter-full-screen', function () { return _this.fullScreen = true; });
                _this.window.on('leave-full-screen', function () { return _this.fullScreen = false; });
            }
            _this.window.on('close', function () {
                if (_this.settings) {
                    _this.settings.destroy();
                    _this.settings = null;
                }
            });
        })
            .on('window-all-closed', function () { electron_1.app.quit(); });
    }
    /** Create a new renderer window. */
    Renderer.prototype.createWindow = function () {
        var _this = this;
        var app = require('electron').app;
        app.setName("YouTubeTV");
        this.window = new electron_1.BrowserWindow({
            width: 1230,
            height: 720,
            titleBarStyle: (0, os_1.platform)() === 'darwin' ? 'hiddenInset' : 'default',
            fullscreen: false,
            fullscreenable: true,
            title: 'YouTube TV',
            backgroundColor: '#282828',
            icon: electron_1.nativeImage.createFromPath((0, path_1.join)((0, process_1.cwd)(), 'build', 'icon.png')),
            webPreferences: {
                nodeIntegration: true,
                webSecurity: true,
                contextIsolation: false
            }
        });
        process.nextTick(function () { return _this.loadSettings(); });
    };
    /**
     * Inject a JavaScript code into the renderer process to patch events and add some features.
     * @param script Type of script to be injected.
     * */
    Renderer.prototype.injectJSCode = function () {
        return __awaiter(this, arguments, void 0, function (script) {
            var _a, _b, error_1;
            if (script === void 0) { script = 'all'; }
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 5, , 6]);
                        if (!(this.jsic === '')) return [3 /*break*/, 2];
                        _a = this;
                        return [4 /*yield*/, (0, promises_1.readFile)((0, path_1.join)(__dirname, 'injection.js'), { encoding: 'utf8' })];
                    case 1:
                        _a.jsic = _c.sent();
                        _c.label = 2;
                    case 2:
                        if (!((0, os_1.platform)() === 'darwin' && this.titleBar === '')) return [3 /*break*/, 4];
                        _b = this;
                        return [4 /*yield*/, (0, promises_1.readFile)((0, path_1.join)(__dirname, 'titleBar.js'), { encoding: 'utf8' })];
                    case 3:
                        _b.titleBar = _c.sent();
                        _c.label = 4;
                    case 4:
                        if (script === 'all') {
                            this.window.webContents.executeJavaScript(this.jsic);
                            (0, os_1.platform)() === 'darwin' ? this.window.webContents.executeJavaScript(this.titleBar) : false;
                        }
                        else if (script === 'patchs') {
                            this.window.webContents.executeJavaScript(this.jsic);
                        }
                        else if (script === 'titlebar') {
                            (0, os_1.platform)() === 'darwin' ? this.window.webContents.executeJavaScript(this.titleBar) : false;
                        }
                        return [3 /*break*/, 6];
                    case 5:
                        error_1 = _c.sent();
                        debugger;
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    Renderer.prototype.setMaxRes = function (params) {
        var width = params.width, height = params.height, reload = params.reload;
        this.localStorageQuery('set', 'maxRes', { width: width, height: height });
        if (reload) {
            this.setResEmulator(width, height);
            this.window.webContents.reload();
        }
        else
            this.updateWindowParams();
    };
    /** Emulate a screen with assigned parameters */
    Renderer.prototype.setResEmulator = function (emuWidth, emuHeight) {
        var _this = this;
        if (emuWidth === void 0) { emuWidth = 3840; }
        if (emuHeight === void 0) { emuHeight = 2160; }
        // Delete all listeners.
        this.window.removeAllListeners('resize');
        // Performs an initial calculation.
        this.calcEmulatedDisplay(emuWidth, emuHeight);
        // Add a listener to the window to recalculate the emulator.
        this.window.on('resize', function () {
            _this.calcEmulatedDisplay(emuWidth, emuHeight);
            _this.updateWindowParams();
        });
    };
    Renderer.prototype.calcEmulatedDisplay = function (emuWidth, emuHeight) {
        // Get the current window size.
        var _a = this.window.getSize(), width = _a[0], height = _a[1];
        this.window.webContents.disableDeviceEmulation();
        this.window.webContents.enableDeviceEmulation({
            screenSize: { width: emuWidth, height: emuHeight },
            viewSize: { width: width / emuWidth, height: height / emuHeight },
            scale: width / emuWidth,
            screenPosition: 'mobile',
            viewPosition: { x: 0.5, y: 0.5 },
            deviceScaleFactor: 0
        });
    };
    /**
     * Listen keyboard shortcuts to perform some actions.
     */
    Renderer.prototype.setAccelerators = function () {
        var _this = this;
        electron_1.globalShortcut.register('ctrl+s', function () {
            if (_this.settings) {
                _this.settings.destroy();
                _this.settings = null;
            }
            else {
                _this.settings = new settings_renderer_1.Settings();
            }
        });
        electron_1.globalShortcut.register('ctrl+f', function () { _this.fullScreen = !_this.window.isFullScreen(); });
        electron_1.globalShortcut.register('ctrl+d', function () { _this.window.webContents.toggleDevTools(); });
        electron_1.globalShortcut.register('ctrl+a', function () { return _this.cursor = null; });
    };
    Renderer.prototype.localStorageQuery = function (type, key, value, data) {
        return __awaiter(this, void 0, void 0, function () {
            var query, unresolvedQuery, resolver, parsed, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(type === 'get' || type === 'set' || type === 'delete' || type === 'clear' || type === 'raw')) return [3 /*break*/, 7];
                        query = 'localStorage.';
                        if (type === 'get')
                            query += "getItem('".concat(key, "')");
                        else if (type === 'set') {
                            if (typeof value === 'object')
                                value = "'".concat(JSON.stringify(value), "'");
                            query += "setItem('".concat(key, "', ").concat(value, ")");
                        }
                        else if (type === 'delete')
                            query += "removeItem('".concat(key, "')");
                        else if (type === 'clear')
                            query += 'clear()';
                        else if (type === 'raw')
                            query = data;
                        unresolvedQuery = this.window.webContents.executeJavaScript(query);
                        if (!(type === 'get')) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, unresolvedQuery];
                    case 2:
                        resolver = _a.sent();
                        parsed = JSON.parse(resolver);
                        return [2 /*return*/, Promise.resolve(parsed)];
                    case 3:
                        error_2 = _a.sent();
                        return [2 /*return*/, unresolvedQuery];
                    case 4: return [3 /*break*/, 6];
                    case 5: return [2 /*return*/, unresolvedQuery];
                    case 6: return [3 /*break*/, 8];
                    case 7: return [2 /*return*/, Promise.reject('unknown query type')];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    Renderer.prototype.listenWindowMoveEvents = function () {
        var _this = this;
        this.window.on('moved', function () { _this.updateWindowParams(); });
    };
    Renderer.prototype.getWindowParams = function () {
        var bounds = this.window.getBounds();
        var fullscreen = this.window.isFullScreen();
        var cursor = this._cursor ? true : false;
        return { bounds: bounds, fullscreen: fullscreen, cursor: cursor };
    };
    Renderer.prototype.updateWindowParams = function () {
        var params = this.getWindowParams();
        this.localStorageQuery('set', 'windowParams', params);
    };
    Renderer.prototype.loadSettings = function () {
        var _this = this;
        this.localStorageQuery('get', 'windowParams')
            .then(function (data) {
            _this.window.setBounds(data.bounds);
            _this.window.fullScreen = data.fullscreen;
            _this.cursor = data.cursor;
            _this.window.on('resized', function () {
                _this.updateWindowParams();
            });
        });
        this.localStorageQuery('get', 'maxRes')
            .then(function (data) {
            // If the usen has not set a resolution, set the default one.
            if (!data)
                _this.setResEmulator();
            else {
                if (data.width && data.height)
                    _this.setResEmulator(data.width, data.height);
                else
                    _this.setResEmulator();
            }
        })
            .catch(function (err) {
            // If the data is invalid or not available, set the default resolution.
            _this.setResEmulator(3840, 2160);
        });
    };
    Object.defineProperty(Renderer.prototype, "url", {
        /**
         * Load new user connection **and reload the renderer process**.\
         * If value is '\_\_DFT\_\_', the default YouTube TV url will be loaded.
         * */
        set: function (value) {
            var _this = this;
            var url = value;
            if (typeof value !== 'string')
                return;
            if (value.length < 1)
                return;
            if (value === '__DFT__')
                url = '';
            this.window.loadURL(this._url + url, { userAgent: this.userAgent })
                .then(function () {
                _this.injectJSCode();
            })
                .catch(function () { return __awaiter(_this, void 0, void 0, function () {
                var offline;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            electron_1.ipcMain.once('restored', function () { _this.url = value; });
                            this.injectJSCode('titlebar');
                            return [4 /*yield*/, (0, promises_1.readFile)((0, path_1.join)(__dirname, 'offline_banner.js'), { encoding: 'utf8' })];
                        case 1:
                            offline = _a.sent();
                            this.window.webContents.executeJavaScript(offline);
                            return [2 /*return*/];
                    }
                });
            }); });
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Renderer.prototype, "urlByDial", {
        set: function (value) {
            var _this = this;
            if (typeof value !== 'string')
                return;
            if (value.length < 1)
                return;
            this.window.fullScreen = true;
            this.window.webContents.loadURL(this._url + value, { userAgent: this.userAgent })
                .then(function () {
                _this.injectJSCode();
            })
                // This should never happen...
                .catch(function () { return __awaiter(_this, void 0, void 0, function () {
                var offline;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            electron_1.ipcMain.once('restored', function () { _this.urlByDial = value; });
                            this.injectJSCode('titlebar');
                            return [4 /*yield*/, (0, promises_1.readFile)((0, path_1.join)(__dirname, 'offline_banner.js'), { encoding: 'utf8' })];
                        case 1:
                            offline = _a.sent();
                            this.window.webContents.executeJavaScript(offline);
                            return [2 /*return*/];
                    }
                });
            }); });
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Renderer.prototype, "fullScreen", {
        set: function (value) {
            if (value === null) {
                this.fullScreen = !this.window.isFullScreen();
                return;
            }
            else {
                if (typeof value !== 'boolean')
                    return;
                this.window.fullScreen = value;
                this.updateWindowParams();
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Renderer.prototype, "cursor", {
        /** Toggle cursor visibility */
        set: function (value) {
            if (typeof value !== 'boolean')
                this._cursor = !this._cursor;
            else
                this._cursor = value;
            if (this._cursor) {
                this.window.webContents.insertCSS('html {cursor: default;}');
            }
            else if (!this._cursor) {
                this.window.webContents.insertCSS('html {cursor: none;}');
            }
            else {
                this.window.webContents.insertCSS('html {cursor: none;}');
            }
            this.updateWindowParams();
        },
        enumerable: false,
        configurable: true
    });
    return Renderer;
}());
exports.Renderer = Renderer;
