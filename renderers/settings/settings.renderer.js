"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Settings = void 0;
var electron_1 = require("electron");
var path_1 = require("path");
var main_1 = require("../../main");
var Settings = /** @class */ (function () {
    function Settings() {
        var _this = this;
        electron_1.app.whenReady().then(function () {
            _this.window = new electron_1.BrowserWindow({
                resizable: false,
                fullscreen: false,
                width: 400,
                height: 500,
                title: 'Settings',
                backgroundColor: '#181818',
                webPreferences: {
                    contextIsolation: false,
                    nodeIntegration: true
                }
            });
            electron_1.ipcMain.on('update', function (_, params) {
                var renderer = main_1.default.renderer;
                renderer.setMaxRes(params);
            });
            electron_1.globalShortcut.register('ctrl+shift+d', function () { _this.window.webContents.toggleDevTools(); });
            _this.window.loadFile((0, path_1.join)(__dirname, 'index.html'));
        });
    }
    Settings.prototype.destroy = function () {
        this.window.destroy();
    };
    return Settings;
}());
exports.Settings = Settings;
