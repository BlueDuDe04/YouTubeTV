"use strict";
/**
 * YouTube TV Desktop Client.
 * Copyright (c) 2021 Marcos Rodríguez Yélamo <marcosylrg@gmail.com>
 *
 * MIT License
 * For more information, visit https://github.com/marcosrg9/YouTubeTV.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var main_renderer_1 = require("./renderers/main/main.renderer");
var DIAL_1 = require("./servers/DIAL");
var Main = /** @class */ (function () {
    function Main() {
        this.renderer = new main_renderer_1.Renderer();
        this.dial = new DIAL_1.Dial();
    }
    return Main;
}());
/** Contains the instance of the main process. */
exports.default = new Main();
